CodeFormater by Niklas Trippler

The code formater is an application that aims to format C/C++ sourcecode (other languages could be added via plugins) to certain standards specified by the application. 
It has support for standards plugins, so if the default behavior is not ideal, plugins can be used to improve it.

Currently works but is to be considered experimental.

Things known to not work:
	"i++ + ++j;" gets formated into "i+++++j;", which is invalid.

Read more at http://trippler.no/wpcms/?page_id=55