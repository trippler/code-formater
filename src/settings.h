/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#ifndef settings_h
#define settings_h
#include <stdbool.h>
struct modulelist;
struct settings{
	char* file;
	int verbose;
	bool preallocTokenlist;
	char* outputfile;
	bool timer;
	struct modulelist* modules;
	bool stdinput;
};

struct settings setting;
#endif
