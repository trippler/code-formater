/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#ifndef FORMATER_C
#define FORMATER_C

#include <stdbool.h>
#include "token.h"
#include "state.h"
#include "state.c"
#include "settings.h"
#include "formaterlines.c"

void format(struct token* before, struct token* current, struct token* next, struct stateStack* statestack, struct tokenlist* outputlist){
	struct formaterState* state = &statestack->state[statestack->stackp];
	if(current == NULL){
		printf("ERROR in formater\r\n");
		return;
	}
	else if(before == NULL){
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == PHFORSTART || current->tokenType == PHIFSTART || current->tokenType == PHWHILESTART || current->tokenType == PHFUNSTART){
		if(current->tokenType != PHFUNSTART)
			state->inPhDefinition = true;
		if(before->tokenType != UNKNOWN && current->tokenType == PHFUNSTART && !(before->tokenType == PHFORSTART || before->tokenType == PHIFSTART || before->tokenType == PHWHILESTART || before->tokenType == PHFUNSTART))
			addToken(outputlist, " ");
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == PHFOREND || current->tokenType == PHIFEND || current->tokenType == PHWHILEEND || current->tokenType == PHFUNEND){
		if(current->tokenType != PHFUNEND)state->inPhDefinition = false;
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == CODEST){
		state = pushStateStack(statestack);
		state->indentationLevel++;
		addToken(outputlist, current->token);
		if(next != NULL && next->tokenType != CODEEND)
			addNewline(state, outputlist);
	}
	else if(current->tokenType == CODEEND){
		if(state->temporaryState)
			state = popStateStackToNonTemp(statestack);
		state = popStateStack(statestack);
		if(outputlist->count > 0 && outputlist->tokens[outputlist->count-1]->tokenType != ENDL)
			addNewline(state, outputlist);
		if(state->temporaryState)
			state = popStateStackToNonTemp(statestack);
		addToken(outputlist, current->token);
		if(next != NULL && next->tokenType != SEMI && next->tokenType != CODEEND){
			addNewline(state, outputlist);
			if(!state->indentationLevel)
				addNewline(state, outputlist);
		}
	}
	else if(current->tokenType == SEMI){
		if(state->inPhDefinition){
			addToken(outputlist, current->token);
		}else if(next != NULL && next->tokenType != CODEEND){
			addToken(outputlist, current->token);
			if(state->temporaryState)
				state = popStateStackToNonTemp(statestack);
			addNewline(state, outputlist);
		}else{
			addToken(outputlist, current->token);
		}
	}
	else if(current->tokenType == COMMA){
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == ENDL){
		if(before->tokenType == TEXT && next != NULL && next->tokenType != CODEEND)
			addNewline(state, outputlist);
	}
	else if(current->tokenType == PLUSONE || current->tokenType == MINUSONE){
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == MULTIEQUALS){
		if(before->tokenType == MULTIEQUALS)
			addToken(outputlist, current->token);
		else{
			addToken(outputlist, " ");
			addToken(outputlist, current->token);
		}
	}
	else if((current->tokenType == COMPAND || current->tokenType == COMPOR) && (before->tokenType == COMPAND || before->tokenType == COMPOR)){
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == ONELINECOMMENT){
		//addNewline(state, outputlist);
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == POINTERTO){
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == COMMENTST){
		if(outputlist->count > 0 && outputlist->tokens[outputlist->count-1]->tokenType != ENDL)addNewline(state, outputlist);
		addToken(outputlist, current->token);
	}else if(current->tokenType == COMMENTEND){
		addToken(outputlist, current->token);
		addNewline(state, outputlist);
	}
	else if(current->tokenType == TEXT || before->tokenType == TEXT){
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == PLUS || current->tokenType == MINUS /*|| current->tokenType == MULT*/ || current->tokenType == DIV || current->tokenType == EQUALS){
		addToken(outputlist, " ");
		addToken(outputlist, current->token);
	}
	else if (before->tokenType == DESTRUCTOR){
		addToken(outputlist, current->token);
	}
	else if(before->tokenType == DOT || current->tokenType == DOT){
		addToken(outputlist, current->token);
	}
	else if(before->tokenType == CLASSDEFINER || current->tokenType == CLASSDEFINER){
		addToken(outputlist, current->token);
	}
	else if(before->tokenType == COMMA){
		addToken(outputlist, " ");
		addToken(outputlist, current->token);
	}
	else if(before->tokenType == POINTERTO){
		addToken(outputlist, current->token);
	}
	else if(before->tokenType == PHFORSTART || before->tokenType == PHIFSTART || before->tokenType == PHWHILESTART || before->tokenType == PHFUNSTART){
		addToken(outputlist, current->token);
	}
	else if((before->tokenType == PHFOREND || before->tokenType == PHIFEND || before->tokenType == PHWHILEEND || (before->tokenType == ELSE && current->tokenType != IF) /* Looks very pretty, but does not follow standard|| before->tokenType == PHFUNEND*/) && current->tokenType != CODEST){
		state = pushStateStack(statestack);
		state->indentationLevel++;
		state->temporaryState = true;
		addNewline(state, outputlist);
		addToken(outputlist, current->token);
	}
	else if(before->tokenType == NOT){
		addToken(outputlist, current->token);
	}
	else if(current->tokenType == ARST || current->tokenType == AREND){
		addToken(outputlist, current->token);
	}
	else if(before->tokenType == ARST || before->tokenType == AREND){
		addToken(outputlist, current->token);
	}
	else if (before->tokenType == PHFUNEND){
		addToken(outputlist, current->token);
	}
	//Assume pointer
	else if (before->tokenType == MULT){
		addToken(outputlist, current->token);
	}
	//Default action
	else{ 
		if(outputlist->count > 0 && outputlist->tokens[outputlist->count-1]->tokenType != ENDL)addToken(outputlist, " ");
		addToken(outputlist, current->token);
	}
}

struct tokenlist* formatList(struct tokenlist* inputlist){
	struct stateStack state = initStack();
	pushStateStack(&state);
	struct tokenlist* outputlist = addToken(NULL, "");
	
	bool preformaterTriggered = false;

	for(int n = 0; n < setting.modules->modulecount; ++n)
		if(setting.modules->modules[n]->preFormater(NULL, inputlist->tokens[0], inputlist->tokens[1], &state, outputlist))
			preformaterTriggered = true;
			
	if(!preformaterTriggered)
		format(NULL, inputlist->tokens[0], inputlist->tokens[1], &state, outputlist);
	
	for(int n = 1; n < inputlist->count; ++n){
		preformaterTriggered = false;
		for(int m = 0; m < setting.modules->modulecount; ++m)
			if(setting.modules->modules[m]->preFormater(inputlist->tokens[n-1], inputlist->tokens[n], (n+1 < inputlist->count)?inputlist->tokens[n+1] : NULL, &state, outputlist))
				preformaterTriggered = true;
		if(!preformaterTriggered)
			format(inputlist->tokens[n-1], inputlist->tokens[n], inputlist->tokens[n+1], &state, outputlist);
	}

	destroyStateStack(&state);
	return outputlist;
}

#endif
