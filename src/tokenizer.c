/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "token.h"

struct tokenlist* findToken(char* buffer, int offset, int buffersize, struct tokenlist* tokens){
	char seperators[] = " \t();{}?*&/[]=,\r\n#<>.\"\'";
	char varKeyspace[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_";
	//char stringContainers[] = "\"\"#\n";

	/*The following variables need to match!*/
	char* strcontainer[][2] = { {"\"", "\""}, {"#", "\n"}, {"/*", "*/"}, {"//", "\n"}, {"\'", "\'"}, {"\\", "\0"} };
	int strcontainers = sizeof(strcontainer)/sizeof(strcontainer[0]);
	
	char* endStringCharacter = 0;
	int pos = 0;
	/*
	0. Not in a string, a \" would start a new string
	1. In a string, \" terminates current token (string content)
	2. In a string, right after end of 1. \" will end string and reset to 0
	*/
	int stringStage = 0;
	while(offset < buffersize){
		pos = 0;
		/*
		Find a non-whitespace character except if in stringStage 2
		*/
		while((buffer[offset] == ' ' || buffer[offset] == '\t' || buffer[offset] == '\r' || buffer[offset] == '\n') && stringStage == 0){
			++offset;
		}
		//If stringStage is not 1 it means we have a "normal" token, find end of it by looking for a char not in the allowed variable keyspace
		if(stringStage != 1){
			pos = strspn(buffer+offset, varKeyspace);
			if(pos == 0)
				++pos;
		}else{
			//If stringStage is 1 it means we are in a string, and looking for the end of the string, search for a '"' that is not led by a '\'
			/*while(!(!strncmp(buffer+offset+pos, endStringCharacter, strlen(endStringCharacter)) && buffer[offset+pos-1] != '\\'))
				++pos;*/
			bool keepLooking = true;
			while(keepLooking){
				//This is a special case where we want to terminate on the next character no matter what
				if(strlen(endStringCharacter) == 0){
					pos += 2;
					keepLooking = false;
					continue;
				}
				if(!strncmp(buffer+offset+pos, endStringCharacter, strlen(endStringCharacter))){
					if(buffer[offset+pos-1] == '\\'){
						int bscount = 0;
						for(int n = 1; buffer[offset+pos-n] == '\\'; ++n)
							++bscount;
						if(bscount % 2 == 0)
							keepLooking = false;
						else
							++pos;
					}else{
						keepLooking = false;
					}
				}else{
					++pos;
				}
			}
		}
		//We have the token starting posision (buffer+offset) and the length (pos), copy it to a seperate string and send to addToken that adds it to list (ST)
		char token[1024] = "";
		if(stringStage == 2)
			pos = strlen(endStringCharacter);
		strncpy(token, buffer+offset, pos);
		
		while(stringStage == 1 && (token[strlen(token)-1] == '\r' || token[strlen(token)-1] == '\n'))
			token[strlen(token)-1] = 0;
		/*if(stringStage == 1)
			printf("String: \"%s\"\r\n", token);
		*/

		//If token is '"' or we are in a string, move to next stringStage.
		//0:Not string->Next Token is String
		//1:InString ->Next token is end of string
		//2:Endof String->Next token is normal token
		if(stringStage == 1)
			stringStage = (stringStage + 1) % 3;
		else{
			if(stringStage == 2){
				stringStage = (stringStage + 1) % 3;
			}
			else{
				for(int n = 0; n < strcontainers; n++)
					if(!strncmp(buffer+offset, strcontainer[n][0], strlen(strcontainer[n][0]))){
						//printf("TL:[%d]", (int)strlen(strcontainer[n][0]));
						//Overwrite last token if the actual token length is larger than the token found
						strncpy(token, buffer+offset, strlen(strcontainer[n][0]));
						//Set updated positioning variable
						pos = strlen(strcontainer[n][0]);
						endStringCharacter = strcontainer[n][1];
						stringStage = (stringStage + 1) % 3;
						break;
					}
			}
		}
		if(stringStage == 2)tokens = addTokenWT(tokens, token, TEXT);
		else tokens = addToken(tokens, token);
		//printf("Token:\'%s\'\r\n", token);
		offset += pos;
	}
	return tokens;
}

/*
Tokens:
if
(
)
;
{
}
[
]
while
for*
*
&
newline

*/
