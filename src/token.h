/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
//#include "settings.h"
#ifndef token_h
#define token_h
typedef enum{
	UNKNOWN,
	TEXT,
	IF, // if
	WHILE, // while
	FOR, // for
	ELSE,
	PARANTHSTART, // (
	PARANTHEND, // )
	COLON, // :
	SEMI, // ;
	COMMA, // ,
	CODEST, // {
	CODEEND, // }
	PLUS, // +
	PLUSONE, //++
	MINUS, // -
	MINUSONE, //--
	MULT, //Æ
	DIV, // /
	OR, //|
	AND, //&
	COMPOR, // ||
	COMPAND, // &&
	ARST, //[
	AREND, //]
	EQUALS,
	MULTIEQUALS, // +=, *= etc
	STR, // "
	NOT, // !
	DOT, // .
	COMMENTST, // /*
	COMMENTEND, // */
	ONELINECOMMENT, // //
	ENDL, // \r\n
	PRAGMA, // #
	LESS, // <
	MORE, // >
	POINTERTO, // ->
	CLASSDEFINER, // ::
	DESTRUCTOR,	//~
	BACKSLASH,
	PHFORSTART,
	PHFOREND,
	PHIFSTART,
	PHIFEND,
	PHWHILESTART,
	PHWHILEEND,
	PHFUNSTART,
	PHFUNEND
} tokentype;

struct token{
        tokentype tokenType;
        char* token;
};

struct tokenlist{
	int count;
	int listsize;
	struct token** tokens;
};


#include "token.c"
#endif

