/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#include <stdio.h>
#include "token.h"
#include "settings.h"

void prettyprint(struct tokenlist* tokens){
	FILE* fp;
	if(!setting.outputfile)
		fp = stdout;
	else
		fp = fopen(setting.outputfile, "wb");
	for(int n = 0; n < tokens->count; ++n){
		fprintf(fp, "%s", tokens->tokens[n]->token);
	}
	if(setting.outputfile)
		fclose(fp);
}
