/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#ifndef STATE_H
#define STATE_H
#include <stdbool.h>

struct formaterState{
	int indentationLevel;
	bool inPhDefinition;
	bool temporaryState;
};

struct stateStack{
	int stackSize;
	int stackp;
	struct formaterState* state;
};

#endif
