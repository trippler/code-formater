/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#ifndef TOKEN_C
#define TOKEN_C
#include "token.h"
#include "settings.h"

struct tokenlist* addTokenWT(struct tokenlist* tokens, char* token, tokentype tokenType){
        //If no tokenlist exists, make one
        if(!tokens){
                tokens = (struct tokenlist*)malloc(sizeof(struct tokenlist));
                tokens->count = 0;
		tokens->listsize = 0;
                tokens->tokens = NULL;
        }
        //Allocate a new token for the list
        struct token* tok = (struct token*)malloc(sizeof(struct token));
        tok->tokenType = tokenType;
        //Allocate memory for the token content (string)
        tok->token = (char*)malloc(strlen(token)+1);
        strncpy(tok->token, token, strlen(token));
        tok->token[strlen(token)] = 0;
        //reallocate to have enough memory for count+1 tokens;
        if(!setting.preallocTokenlist)
		tokens->tokens = realloc(tokens->tokens, ++(tokens->count)*sizeof(struct token));
	else{
		if(tokens->listsize <= tokens->count){
			if(tokens->listsize < 4)
				tokens->listsize = 4;
			tokens->listsize = (1.615*tokens->listsize);
			tokens->tokens = realloc(tokens->tokens, tokens->listsize*sizeof(struct token));
		}
		++(tokens->count);
	}
        if(tokens->tokens == NULL){
                printf("Error: could not allocate memory!");
                exit(1);
        }
        tokens->tokens[tokens->count-1] = tok;
        return tokens;
}

struct tokenlist* addToken(struct tokenlist* tokens, char* token){
        return addTokenWT(tokens, token, UNKNOWN);
}

void freeTokenlist(struct tokenlist* tokens){
        if(!tokens){
                printf("Error: tokenlist is NULL.\r\n");
                return;
        }
        for(int count = tokens->count-1; count >= 0; --count){
                //printf("Freeing element [%d].\r\n", count);
                free(tokens->tokens[count]->token);
                free(tokens->tokens[count]);
        }
        free(tokens->tokens);
        free(tokens);
        if(setting.verbose >= 2)
		printf("Deallocated entire tokenlist and all elements contained within.\r\n");
}

#endif
