/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>

#include "settings.h"
#include "module.h"
#include "token.h"
#include "tokenizer.c"
#include "analyser.c"
#include "formater.c"
#include "prettyprint.c"


struct settings initializeArguments(int argc, char* argv[]);

int main(int argc, char* argv[]){
	setting = initializeArguments(argc, argv);
	
	struct timespec starttime;
	if(setting.timer)
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &starttime);

	if(!setting.file && !setting.stdinput){
		printf("Error: no file specified!\r\n");
		return 1;
	}

	FILE* filep;
	if(setting.stdinput)
		filep = stdin;
	else
		filep = fopen(setting.file, "rb");
	if(!filep){
		printf("Error file \"%s\" does not exist.\r\n", setting.file);
		return 1;
	}

	//Find file size
	struct stat st;
	int filesize;
	if(!setting.stdinput)
		stat(setting.file, &st);
	filesize = (!setting.stdinput) ? st.st_size : 32768;

	if(setting.verbose)
		printf("File size: %d\r\n", filesize);

	//Allocate memory for file content
	char* filebuffer = malloc(filesize+1);
	filebuffer[filesize] = 0;
	for(int n = 0; n < filesize;){
		int size = fread(filebuffer+n, 1, filesize, filep);
		if(size <= 0){
			printf("Error when reading file!\r\n");
			break;
		}
		n+= size;
		if(setting.verbose)
			printf("Read %d bytes.\r\n", size);
	}

	if(setting.verbose)
		printf("Content of file:\r\n-------------------------\r\n%s-------------------------\r\n", filebuffer);
	if(setting.verbose)
		printf("Tokenizing memory\r\n");
	struct tokenlist* tokens = findToken(filebuffer, 0, filesize, NULL);
	classify(tokens);
	for(int n = 0; n < tokens->count; n++){
		if(tokens->tokens[n]->tokenType == UNKNOWN && setting.verbose == 2)
			printf("Unknown tokentype: \'%s\'\r\n", tokens->tokens[n]->token);
	}
	struct tokenlist* outputTokens;
	outputTokens = formatList(tokens);
	prettyprint(outputTokens);
	if(setting.timer){
		struct timespec endtime;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &endtime);
		long timediff = endtime.tv_nsec - starttime.tv_nsec;
		printf("Finished in %ld ns (%lf seconds)\r\n", timediff, (double)timediff/(double)(1000000000));
	}
	if(setting.verbose)
		printf("Deallocatig memory\r\n");
	freeTokenlist(tokens);
	freeTokenlist(outputTokens);
	/*
	Cleanup 
	*/
	if(setting.file)
		free(setting.file);
	if(setting.outputfile)
		free(setting.outputfile);
	fclose(filep);
	free(filebuffer);
	freeModuleList(setting.modules);
	pthread_exit(NULL);
	return 0;
}

struct settings initializeArguments(int argc, char* argv[]){
	int arg = 1;
	setting.file = NULL;
	setting.verbose = 0;
	setting.preallocTokenlist = false;
	setting.modules = malloc(sizeof(struct modulelist));
	setting.modules->modulecount = 0;
	setting.modules->modulestacksize = 0;
	setting.modules->modules = malloc(4);

	while(arg < argc){
		if((!strcmp(argv[arg], "--file") || !strcmp(argv[arg], "-file")) && argc > arg+1){
			if(setting.verbose)
				printf("Setting file to: %s\r\n", argv[arg+1]);
			if(setting.file != NULL)
				free(setting.file);
			setting.file = (char*)malloc(strlen(argv[++arg])+1);
			memcpy(setting.file, argv[arg], strlen(argv[arg]));
			setting.file[strlen(argv[arg])] = 0;
		}
		else if((!strcmp(argv[arg], "--std") || !strcmp(argv[arg], "-std") || !strcmp(argv[arg], "--standard")) && argc > arg+1){
			char fullPath[1024] = "";
			snprintf(fullPath, 1023, "stds/%s.so", argv[arg+1]);
			if(!addModule(fullPath, setting.modules))
				printf("Error: something wrong with module: %s!\r\n", argv[arg+1]);
			arg++;
		}
		else if((!strcmp(argv[arg], "--o") || !strcmp(argv[arg], "-o") || !strcmp(argv[arg], "--output") || !strcmp(argv[arg], "-output")) && argc > arg+1){
			if(setting.verbose)
				printf("Setting output to: %s\r\n", argv[arg+1]);
			if(setting.outputfile != NULL)
				free(setting.file);
			setting.outputfile = (char*)malloc(strlen(argv[++arg])+1);
			memcpy(setting.outputfile, argv[arg], strlen(argv[arg]));
			setting.outputfile[strlen(argv[arg])] = 0;
		}
		else if(!strcmp(argv[arg], "-v") || !strcmp(argv[arg], "--v") || !strcmp(argv[arg], "--verbose")){
			setting.verbose = 1;
		}
		else if(!strcmp(argv[arg], "-vv") || !strcmp(argv[arg], "--vv") || !strcmp(argv[arg], "--verbose2")){
			setting.verbose = 2;
		}
		else if(!strcmp(argv[arg], "-speedalloc") || !strcmp(argv[arg], "--speedalloc") || !strcmp(argv[arg], "--prealloc")){
			setting.preallocTokenlist = true;;
		}
		else if(!strcmp(argv[arg], "--help") || !strcmp(argv[arg], "-h") || !strcmp(argv[arg], "--?") || !strcmp(argv[arg], "-?")){
			printf(
			"\r\n"
			"CodeFormater 0.1 by Niklas Trippler\r\n\r\n"
			"--file [-file] <file>\r\n\t"
			"File to format\r\n"
			
			"--output [--o, -o, -output] <file>\r\n\t"
			"File where output will be written\r\n"

			"--verbose [-v --v]\r\n\t"
			"Verbosety level 1 / Verbose\r\n"

			"--verbose2 [--vv, -vv]\r\n\t"
			"Verbosity level 2 / Very verbose\r\n"

			"--prealloc [-speedalloc, --speedalloc]\r\n\t"
			"Allocate larger blocks of memory instead of when needed. This option will increase speed and decrease memory allocation, but increase memory usage\r\n"
			
			"--help [-h, --?, -?]\r\n\t"
			"This help document\r\n"

			"--timer [-timer]\r\n\t"
			"Measure the time it takes to run the program\r\n"
			);
			exit(0);
		}
		else if(!strcmp(argv[arg], "--timer") || !strcmp(argv[arg], "-timer")){
			setting.timer = true;
		}
		else if(!strcmp(argv[arg], "--stdinput") || !strcmp(argv[arg], "-stdinput")){
			setting.stdinput = true;
		}
		arg++;
	}
	return setting;
}
