/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#include "token.h"
#include <string.h>

void bind(char* str, tokentype type, struct tokenlist* tokens){
	for(int n = 0; n < tokens->count; n++){
		if(!strcmp(tokens->tokens[n]->token, str)){
			tokens->tokens[n]->tokenType = type;
		}
	}
}

void bindList(struct tokenlist* tokens){
	bind("if", IF, tokens);
	bind("while", WHILE, tokens);
	bind("for", FOR, tokens);
	bind("else", ELSE, tokens);
	bind("(", PARANTHSTART, tokens);
	bind(")", PARANTHEND, tokens);
	bind(";", SEMI, tokens);
	bind(",", COMMA, tokens);
	bind("{", CODEST, tokens);
	bind("}", CODEEND, tokens);
	bind("+", PLUS, tokens);
	bind("-", MINUS, tokens);
	bind("*", MULT, tokens);
	bind("/", DIV, tokens);
	bind("=", EQUALS, tokens);
	bind("|", OR, tokens);
	bind("&", AND, tokens);
	bind("[", ARST, tokens);
	bind("]", AREND, tokens);
	bind("\"", STR, tokens);
	bind("!", NOT, tokens);
	bind(".", DOT, tokens);
	bind(":", COLON, tokens);
	bind("/*", COMMENTST, tokens);
	bind("*/", COMMENTEND, tokens);
	bind("//", ONELINECOMMENT, tokens);
	bind("\n", ENDL, tokens);
	bind("#", PRAGMA, tokens);
	bind("<", LESS, tokens);
	bind(">", MORE, tokens);
	bind("~", DESTRUCTOR, tokens);
	bind("\\", BACKSLASH, tokens);
}

int findMatchingParanthesis(int startPh, struct tokenlist* tokens){
	for(int pos = startPh+1; pos < tokens->count; ++pos){
		if(tokens->tokens[pos]->tokenType == PARANTHEND)
			return pos;
		else if(tokens->tokens[pos]->tokenType == PARANTHSTART){
			pos = findMatchingParanthesis(pos, tokens);
			if(pos == -1)
				return -1;
		}
	}
	printf("ERROR: Should never reach this part in valid code!\r\n");
	return -1;
}

void classify(struct tokenlist* tokens){
	bindList(tokens);
	for(int n = 0; n < tokens->count; ++n){
		if(tokens->tokens[n]->tokenType == PARANTHSTART){
			int pos = findMatchingParanthesis(n, tokens);
			if(pos == -1){
				printf("ERROR!");
				return;
			}
			if(tokens->tokens[n-1]->tokenType == FOR){
				tokens->tokens[n]->tokenType = PHFORSTART;
				tokens->tokens[pos]->tokenType = PHFOREND;
			}
			else if(tokens->tokens[n-1]->tokenType == IF){
				tokens->tokens[n]->tokenType = PHIFSTART;
				tokens->tokens[pos]->tokenType = PHIFEND;
			}
			else if(tokens->tokens[n-1]->tokenType == WHILE){
				tokens->tokens[n]->tokenType = PHWHILESTART;
				tokens->tokens[pos]->tokenType = PHWHILEEND;
			}
			else{
				tokens->tokens[n]->tokenType = PHFUNSTART;
				tokens->tokens[pos]->tokenType = PHFUNEND;
			}
		}
		else if(tokens->tokens[n]->tokenType == PLUS && tokens->tokens[n-1]->tokenType == PLUS){
			tokens->tokens[n]->tokenType = PLUSONE;
			tokens->tokens[n-1]->tokenType = PLUSONE;
		}
		else if(tokens->tokens[n]->tokenType == MINUS && tokens->tokens[n-1]->tokenType == MINUS){
			tokens->tokens[n]->tokenType = MINUSONE;
			tokens->tokens[n-1]->tokenType = MINUSONE;
		}
		else if((tokens->tokens[n]->tokenType == EQUALS) && (tokens->tokens[n-1]->tokenType == MINUS || tokens->tokens[n-1]->tokenType == PLUS || tokens->tokens[n-1]->tokenType == DIV || tokens->tokens[n-1]->tokenType == MULT || tokens->tokens[n-1]->tokenType == NOT || tokens->tokens[n-1]->tokenType == OR || tokens->tokens[n-1]->tokenType == LESS || tokens->tokens[n-1]->tokenType == MORE || tokens->tokens[n-1]->tokenType == EQUALS)){
			tokens->tokens[n]->tokenType = MULTIEQUALS;
			tokens->tokens[n-1]->tokenType = MULTIEQUALS;
		}
		else if(tokens->tokens[n]->tokenType == OR && tokens->tokens[n-1]->tokenType == OR){
			tokens->tokens[n]->tokenType = COMPOR;
			tokens->tokens[n-1]->tokenType = COMPOR;
		}
		else if(tokens->tokens[n]->tokenType == COLON && tokens->tokens[n-1]->tokenType == COLON){
			tokens->tokens[n]->tokenType = CLASSDEFINER;
			tokens->tokens[n-1]->tokenType = CLASSDEFINER;
		}
		else if(tokens->tokens[n]->tokenType == AND && tokens->tokens[n-1]->tokenType == AND){
			tokens->tokens[n]->tokenType = COMPAND;
			tokens->tokens[n-1]->tokenType = COMPAND;
		}
		else if(tokens->tokens[n]->tokenType == MORE && tokens->tokens[n-1]->tokenType == MINUS){
			tokens->tokens[n]->tokenType = POINTERTO;
			tokens->tokens[n-1]->tokenType = POINTERTO;
		}
	}
}
