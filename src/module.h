/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#include <dlfcn.h>
#ifndef MODULE_H
#define MODULE_H

struct token;
struct stateStack;
struct tokenlist;

struct module{
	void* libhandle;
	bool (*preFormater)(struct token* before, struct token* current, struct token* next, struct stateStack* statestack, struct tokenlist* outputlist);
};

struct modulelist{
	int modulecount;
	int modulestacksize;
	struct module** modules;
};
#include "module.c"
#include "token.h"
#include "state.h"
#endif

