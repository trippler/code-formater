#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../state.h"
#include "../state.c"
#include "../token.h"
#include "../token.c"
#include "../formaterlines.c"

bool preformat(struct token* before, struct token* current, struct token* next, struct stateStack* statestack, struct tokenlist* outputlist){
	struct formaterState* state = &statestack->state[statestack->stackp];
	
	if(state->indentationLevel && outputlist->count > 0 && outputlist->tokens[outputlist->count-1]->tokenType == ENDL && before->tokenType == CODEEND && current->tokenType != CODEEND){
		addNewline(state, outputlist);
		return false;
	}
	else 
		return false;
	return true;
}
