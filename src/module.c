/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#include <dlfcn.h>
#include "module.h"

bool addModule(char* path, struct modulelist* list){
	if(list->modulestacksize <= list->modulecount){
		list->modulestacksize = (list->modulestacksize*2)+1;
		list->modules = realloc(list->modules, list->modulestacksize*sizeof(struct module));
	}

	struct module* current = malloc(sizeof(struct module));
	current->libhandle = 0;
	current->preFormater = 0;

	current->libhandle = dlopen(path, RTLD_LAZY);
	if(!current->libhandle){
		printf("Error: could not load library %s\r\n", path);
		return false;
	}

	current->preFormater = dlsym(current->libhandle, "preformat");
	if(dlerror() != NULL){
		printf("Error: %s did not contain the function preformat()\r\n", path);
		return false;
	}
	list->modules[list->modulecount++] = current;
	
	return true;
}

void freeModuleList(struct modulelist* list){
	for(int n = 0; n < list->modulecount; ++n){
		dlclose(list->modules[n]->libhandle);
		free(list->modules[n]);
	}
	free(list->modules);
	free(list);
}
