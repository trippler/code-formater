/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/

//Use \r\n on windows, use \n on everything else
#ifdef WIN32
char* _ENDL = "\r\n";
#else
char* _ENDL = "\n";
#endif


void addIndents(struct formaterState* state, struct tokenlist* outputlist){
        char token[1024];
        for(int n = 0; n < state->indentationLevel; ++n)
                token[n] = '\t';
        token[state->indentationLevel] = 0;
        addTokenWT(outputlist, token, ENDL);
}

void addNewline(struct formaterState* state, struct tokenlist* outputlist){
        addTokenWT(outputlist, _ENDL, ENDL);
        addIndents(state, outputlist);
}

