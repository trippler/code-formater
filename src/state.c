/*
You may use this code for any purpose.
You may make any modifications, but leave this comment in here.
Author: Niklas Trippler
*/
#ifndef STATE_C
#define STATE_C

#include <stdbool.h>
#include "state.h"

struct stateStack initStack(){
	struct stateStack stack;
	stack.stackSize = 0;
	stack.stackp = -1;
	stack.state = NULL;
	return stack;
}

struct formaterState* pushStateStack(struct stateStack* stack){
	if(stack->stackp+1 <= stack->stackSize){
		stack->state = realloc(stack->state, sizeof(struct formaterState)*(++stack->stackSize));
	}

	stack->stackp++;

	if(stack->stackp == 0){
		stack->state[stack->stackp].indentationLevel = 0;
		stack->state[stack->stackp].inPhDefinition = false;
		stack->state[stack->stackp].temporaryState = false;
		return &(stack->state[stack->stackp]);
	}
	stack->state[stack->stackp].indentationLevel = stack->state[stack->stackp-1].indentationLevel;
	stack->state[stack->stackp].inPhDefinition = stack->state[stack->stackp-1].inPhDefinition;
	stack->state[stack->stackp].temporaryState = false;
	return &(stack->state[stack->stackp]);
}

struct formaterState* popStateStack(struct stateStack* stack){
	if(stack->stackp <= 0){
		printf("Warning: stackpointer <= 0!\r\n");
	}
	return &(stack->state[--(stack->stackp)]);
}

struct formaterState* popStateStackToNonTemp(struct stateStack* stack){
	struct formaterState* state = popStateStack(stack);
	while(state->temporaryState)
		state = popStateStack(stack);
	return state;
}

void destroyStateStack(struct stateStack* stack){
	free(stack->state);
	stack->state = NULL;
}

#endif
