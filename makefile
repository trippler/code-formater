all: CodeFormat lib
CodeFormat:
	gcc -g --std=c99 src/main.c -lrt -ldl -lpthread -o formater
lib: 
	gcc --shared --std=c99 src/stds/first.c -o stds/first.so -fPIC
	gcc --shared --std=c99 src/stds/toplevelSpacing.c -o stds/spacedtoplevel.so -fPIC
	gcc --shared --std=c99 src/stds/spacedcodeend.c -o stds/spacedcode.so -fPIC
run: CodeFormat lib
	./formater --file dummydata.c -o output.c --std first
time: CodeFormat
	./formater --file dummydata.c --output output.c --timer
show: CodeFormat lib
	./formater --file dummydata.c -o output.c --std first --std spacedtoplevel --std spacedcode
	gedit dummydata.c output.c
